1. This project should compile and run as an Android application without any extra configuration.
The iBICOOP library is already included as a JAR in the "./lib" folder

2. To recompile iBICOOP please import the following projects in your current workspace:
- IbicoopCore
- IbicoopCoreSE
- IbicoopCoreAndroid
- IbicoopCoreDesktop
- IbicoopExternalLib
- IbicoopExternalLibSE

3. Configure ibicoop_build.properties depending on your workspace file structure.

4. Run ibicoop_build.xml Ant Task

