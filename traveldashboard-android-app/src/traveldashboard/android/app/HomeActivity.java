package traveldashboard.android.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class HomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		IbicoopStartStopTask ibiStart = new IbicoopStartStopTask(this, false, null);
		ibiStart.execute();
		
	}
	
	@Override
	protected void onDestroy(){
		IbicoopStartStopTask ibiStart = new IbicoopStartStopTask(this, true, null);
		ibiStart.execute();
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_home, menu);
		return true;
	}

}
