package traveldashboard.android.app;

import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.platform.sysinfo.SystemInfoNative;
import org.ibicoop.sdp.manager.DiscoveryManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * An AsyncTask to manage the start & stop of iBICOOP
 * 
 * @author andriesc
 *
 */
public class IbicoopStartStopTask extends AsyncTask<Void, Void, Boolean> {

    private Context context;
    private boolean isStopping;
    private ProgressDialog progress;

    public IbicoopStartStopTask(Context ctx, boolean isStopping, ProgressDialog progress){
            this.context = ctx;
            this.isStopping = isStopping;
            
            if(!isStopping){
            	this.progress = progress;
            }
    }
    
    @Override
    protected Boolean doInBackground(Void... params) {
            // stopping iBICOOP
            try{
                    if(isStopping){
                            /* Terminate iBICOOP */
                            IbicoopInit.getInstance().terminate();
                            return true;
                    }
            } catch (Exception e) {
            	e.printStackTrace();
            	return false;
            }
            
            // starting iBICOOP
            try{
            		String IBICOOP_USERNAME = "TravelDashboardAndroid@ibicoop.org";
            		String IBICOOP_PASSWORD = "TravelDashboard";
                    
                    SystemInfoNative sysInfo = new SystemInfoNative(
                                    context.getApplicationContext());
                    Object[] initObjects = { context.getApplicationContext(),
                                    IBICOOP_USERNAME, IBICOOP_PASSWORD,
                                    sysInfo.getDeviceUniqueId() };
                    IbicoopInit.getInstance().start(initObjects);
                    BrokerManager brokerManager = IbicoopInit.getInstance().getBrokerManager("TravelDashboard");
                    DiscoveryManager discoveryManager = IbicoopInit.getInstance().getDiscoveryManager();

                    // Start receiving event notifications
                    //if (!brokerManager.startNotification(eventListener)) {
                    //        throw new IOException();
                    //}

                    // Wait until the broker is started
//                    if (brokerManager.getBrokerStatus() < 0) {
//                            throw new IOException();
//                    }
                    
//                    long brokerStartTime = System.currentTimeMillis();
//                    while(brokerManager.getBrokerStatus() != BrokerConstants.STATUS_BROKER_STARTED_WITH_NOTIFICATION){
//                            try {
//                                    Thread.sleep(500);
//                            } catch (InterruptedException e) {
//                                    
//                            }
//                            if(System.currentTimeMillis() - brokerStartTime >= BROKER_START_TIMEOUT_SECONDS * 1000){
//                                    if (Constants.isDebugEnabled)
//                                            Log.e(Constants.logErrorTag, "Broker-start timeout!");
//                                    throw new IOException();
//                            }
//                    }
                    return true;
            } catch (Exception e) {
                    return false;
            }
    }
    
    protected void onPostExecute(Boolean ok) {
            if(isStopping)
                    // GUI is probably not displayed, will not show any message
                    return;
            
            if(progress != null)
            	progress.dismiss();
            
            if(!ok)
            	Toast.makeText(context, "Failed to start iBICOOP", Toast.LENGTH_LONG).show();
            else {
            	Toast.makeText(context, "iBICOOP start OK", Toast.LENGTH_LONG).show();
            	
            	// iBICOOP communication should always be done in a background thread.
        		new Thread(new Runnable() {
        			@Override
        			public void run() {
        				HelloTdClient client = new HelloTdClient();
        				client.sendHello();
        			}
        		}).start();
            }
    }

}
