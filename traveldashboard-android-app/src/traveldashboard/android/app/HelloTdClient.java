package traveldashboard.android.app;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.communication.common.SenderListener;
import org.ibicoop.exceptions.ConnectionFailedException;
import org.ibicoop.exceptions.MalformedIbiurlException;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.naming.IBIURL;

/**
 * Sends a Hello message to the TravelDashboard Server
 * 
 * @author andriesc
 *
 */
public class HelloTdClient {
	private SenderListener dummySenderListener =new SenderListener() {
		
		@Override
		public void receivedMessageResponse(IbiSender sender, String receiverId,
				int requestId, byte[] data) {
		}
		
		@Override
		public void connectionStatus(IbiSender sender, int statusCode,
				String statusMessage) {
			System.out.println("iBICOOP Connection status: " + statusMessage + ", code=" + statusCode );
		}
	};
	
	public void sendHello(){
		try {
			IBIURL uriLocal = new IBIURL("ibiurl", "TravelDashboard@ibicoop.org",
					"Android", "TravelDashboard", "Test", "1");
			IBIURL uriRemote = new IBIURL("ibiurl", "TravelDashboard@ibicoop.org",
					"Server", "TravelDashboard", "Test", "1");
			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit
					.getInstance()
					.getCommunicationManager();
			IbiSender sender = comm
					.createSender(uriLocal, uriRemote, options, dummySenderListener);
			
			sender.send("Hello world iBICOOP!".getBytes());
		} catch (ConnectionFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedIbiurlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
